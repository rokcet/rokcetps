import React from 'react';
import { compose } from 'recompose';

import { withAuthorization, withEmailVerification } from '../Session';
import ImageUpload from "../ImageUpload/ImageUpload";

const HomePage = () => (
    <div>
        <h1>Home Page</h1>
        <p>The Home Page is accessible by everyone.</p>
        <ImageUpload/>

    </div>
);

const condition = authUser => !!authUser;

export default compose(
    withAuthorization(condition),
)(HomePage);