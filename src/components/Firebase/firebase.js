import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/firestore';
import 'firebase/storage';

const config = {
    apiKey: "AIzaSyD3uAqW92-0HZjHEUOxSwoAqO88IKea1n8",
    authDomain: "rokcet-210502.firebaseapp.com",
    databaseURL: "https://rokcet-210502.firebaseio.com",
    projectId: "rokcet-210502",
    storageBucket: "rokcet-210502.appspot.com",
    messagingSenderId: "807859715422"
};

class Firebase {
    constructor() {
        app.initializeApp(config);

        /* Helper */

        this.serverValue = app.database.ServerValue;
        this.emailAuthProvider = app.auth.EmailAuthProvider;

        /* Firebase APIs */

        this.auth = app.auth();
        this.db = app.firestore();
        this.storage = app.storage();

        /* Social Sign In Method Provider */

        this.googleProvider = new app.auth.GoogleAuthProvider();
        this.facebookProvider = new app.auth.FacebookAuthProvider();
        this.twitterProvider = new app.auth.TwitterAuthProvider();
    }

    // *** Auth API ***

    doCreateUserWithEmailAndPassword = (email, password) =>
        this.auth.createUserWithEmailAndPassword(email, password);

    doSignInWithEmailAndPassword = (email, password) =>
        this.auth.signInWithEmailAndPassword(email, password);

    doSignOut = () => this.auth.signOut();

    doPasswordReset = email => this.auth.sendPasswordResetEmail(email);

    doSendEmailVerification = () =>
        this.auth.currentUser.sendEmailVerification({
            url: 'http://localhost:3000/signin'//process.env.REACT_APP_CONFIRMATION_EMAIL_REDIRECT,
        });

    doPasswordUpdate = password =>
        this.auth.currentUser.updatePassword(password);

    // *** Merge Auth and DB User API *** //

    onAuthUserListener = (next, fallback) =>
        this.auth.onAuthStateChanged(authUser => {
            if (authUser) {
                if(authUser.isAnonymous){
                    //next({...authUser, uid: 'anon'});
                    authUser = {
                        uid: 'anon',
                        email: authUser.email,
                        emailVerified: authUser.emailVerified,
                        providerData: authUser.providerData,
                        isAnonymous: authUser.isAnonymous,
                        role: 3
                    };
                    next(authUser);
                }
                else {
                    this.user(authUser.uid)
                        .get()
                        .then(snapshot => {
                            const userData = snapshot.data();

                            // merge auth and db user
                            authUser = {
                                uid: authUser.uid,
                                email: authUser.email,
                                emailVerified: authUser.emailVerified,
                                providerData: authUser.providerData,
                                isAnonymous: authUser.isAnonymous,
                                ...userData
                            };

                            next(authUser);
                        });
                }
            }
            else {
                this.auth.signInAnonymously().catch(error => {
                        console.log(error.code + ' ' + error.message);
                        fallback();
                    }
                )
            }
        });

    // *** User API ***
    user = uid => this.db.collection('users').doc(uid);
    users = () => this.db.collection('users');

}

export default Firebase;