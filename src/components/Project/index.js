import React, { Component } from 'react';
import { withAuthorization, withEmailVerification } from '../Session';
import { compose } from 'recompose';
import withAuthentication from "../Session/withAuthentication";

class ProjectPage extends Component {
    constructor(props){
        super(props);

        //for now, we'll just connect it to the server and download the porject

        const {pid} = this.props.match.params;
        //const {}

        this.state = {
            pid: pid,
            //project: this.props.project || {
                //photo_f, photo_i, request, status, submitter, tasker
                photoF: '',
                photoI: '',
                request: '',
                status: '',
                submitter: '',
                tasker: ''
            //}
        };


        this.onChange = this.onChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        //this.connectProjectToSubmitter = this.connectProjectToSubmitter.bind(this);

        //get id from url
        this.props.firebase.db.collection('projects').doc(pid)
            .onSnapshot(this.loadProject);
    }

    loadProject = (doc) => {
        //console.log(doc);
        //console.log(doc.data());
        this.setState(doc.data());//{
        console.log(this.state);
            //project: doc.data()
        //});
    };

    onChange(e){
        e.preventDefault();
        //this.setState({file: e.target.files[0]});
    }

    handleClick(e){
        e.preventDefault();
    }

    render() {
        /*
            So return the project name, the before and after image (if present),
            with the user that submitted and the user that tasked (if present)
            and the description
         */
        //console.log(this.props);
        //console.log(this.state);
        return (
            <div>
                <p>{this.state.photoF}</p>
                <p>{this.state.photoI}</p>
                <p>{this.state.request}</p>
                <p>{this.state.status}</p>
            </div>
        );
    }
}

//submitted anonymously, or the user owns it
const condition = authUser => !!authUser;

//export default ImageUpload;
export default compose(
    withAuthentication,
    withAuthorization(condition),
)(ProjectPage);
