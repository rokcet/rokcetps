import React, { Component } from 'react';
//import firebase from '../Firebase';
import { withAuthorization, withEmailVerification } from '../Session';
import { compose } from 'recompose';
import uuid from 'uuid';
import withAuthentication from "../Session/withAuthentication";

class ImageUpload extends Component {
    constructor(props){
        super(props);
        this.state = {
            file: null,
            url: '',
            request: ''
        };

        this.onChange = this.onChange.bind(this);
        this.onTextChange = this.onTextChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.connectProjectToSubmitter = this.connectProjectToSubmitter.bind(this);
    }

    onChange(e){
        e.preventDefault();
        this.setState({file: e.target.files[0]});
    }

    onTextChange(e){
        e.preventDefault();
        this.setState({request: e.target.value});
    }

    handleClick(e){
        e.preventDefault();
        let {file} = this.state;

        const randomName = uuid.v4();
        //probably have project id here later
        const ref = this.props.firebase
            .storage
            .ref('images')
            .child(randomName);

        const uploadTask = ref.put(file);
        uploadTask.on('state_changed',
            snapshot => {
                //progress
                console.log(snapshot.bytesTransferred);
            },
            error => {
                console.log(error);
            }
        );

        let newProject = {
            photoI: randomName,
            photoF: '',
            request: this.state.request,
            status: 'not_started',
            submitter: new this.props.firebase.user(this.props.authUser.uid),
            tasker: ''
        };

        this.props.firebase.db.collection('projects').add(newProject)
            .then(docRef => {
                this.connectProjectToSubmitter(docRef);

                //consider passing the project to projectpage
                this.props.history.push('/p/' + docRef.id);
            })
            .catch(error => {
                console.log(error);
            })

    }

    connectProjectToSubmitter(docRef){
        //we need to load, append, and save
        const userRef = this.props.firebase.user(this.props.authUser.uid);
        userRef.get().then(snapshot => {
            if(snapshot.data().submitted)
                userRef.set({submitted: snapshot.data().submitted.concat(docRef)}, {merge: true});
            else
                console.log('what the fuck');
        });
    }

    render() {
        return (
            <div>
                <input type='file' onChange={e => this.onChange(e)}/>
                <input type='button' onClick={e => this.handleClick(e)} value={'upload'}/>
                <input type='text' onChange={e => this.onTextChange(e)}/>
                <p>{this.state.url}</p>
            </div>
        );
    }
}

//anyone authenticated, even anons
const condition = authUser => !!authUser;

//export default ImageUpload;
export default compose(
    withAuthentication,
    withAuthorization(condition),
)(ImageUpload);
